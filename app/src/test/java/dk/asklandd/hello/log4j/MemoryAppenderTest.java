package dk.asklandd.hello.log4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.lookup.StrLookup;
import org.apache.logging.log4j.core.lookup.StrSubstitutor;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class MemoryAppenderTest
{

    @Test
    public void shouldHavePropertyOfAppLogPattern ()
    {
        // Arrange.
        // Act.
        LoggerContext loggerContext = LoggerContext.getContext();
        Configuration configuration = loggerContext.getConfiguration();
        StrSubstitutor strSubstitutor = configuration.getStrSubstitutor();
        StrLookup variableResolver = strSubstitutor.getVariableResolver(); // https://stackoverflow.com/a/37689415/2412477
        String pattern = variableResolver.lookup("app.log.pattern");
//        PatternLayout patternLayout = PatternLayout.newBuilder().withPattern(pattern).build();

        // Assert.
        assertThat(pattern, containsString("yyyy-MM-dd HH:mm:ss.SSS"));
    }

    @Test
    public void shouldAddMemoryAppenderToRootLogger ()
    {
        // Arrange.
        // Act.
        LoggerContext loggerContext = LoggerContext.getContext();
        Logger rootLogger = loggerContext.getRootLogger();
        Map<String, Appender> appenders = rootLogger.getAppenders();

        // Assert.
        assertThat(appenders.keySet(), hasItems("HelloMemoryAppender"));
    }

    @Test
    public void shouldContainLogEntryInMemoryAppenderUsingLog4j () throws InterruptedException
    {
        // Arrange.
        Logger rootLogger = (Logger)LogManager.getRootLogger();
        MemoryAppender memoryAppender = (MemoryAppender)rootLogger.getAppenders().get("HelloMemoryAppender");

        org.apache.logging.log4j.Logger Log = org.apache.logging.log4j.LogManager.getLogger(App.class);

        // Act.
        Log.error("Oops!");
//        Thread.sleep(100);

        // Assert.
        assertEquals(1, memoryAppender.getEvents().size());
        assertEquals("Oops!", memoryAppender.getEvents().stream().map(ev -> ev.getMessage().getFormattedMessage()).findFirst().orElse(null));
    }

//    @Test
//    public void shouldContainLogEntryInMemoryAppenderUsingSlf4j () throws InterruptedException
//    {
//        // Arrange.
//        LoggerContext loggerContext = LoggerContext.getContext();
//        Logger rootLogger = loggerContext.getRootLogger();
//        MemoryAppender memoryAppender = (MemoryAppender)rootLogger.getAppenders().get("HelloMemoryAppender");
//
//        org.slf4j.Logger Log = org.slf4j.LoggerFactory.getLogger(App.class);
//
//        // Act.
//        Log.error("Oops!");
//        Thread.sleep(100);
//
//        // Assert.
//        assertEquals( 1, memoryAppender.getEvents().size() );
//        assertEquals( "Oops!", memoryAppender.getEvents().stream().map( ev -> ev.getMessage().getFormattedMessage() ).findFirst().orElse(null) );
//    }

}
