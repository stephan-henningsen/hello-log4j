package dk.asklandd.hello.log4j;

import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.core.lookup.StrLookup;
import org.apache.logging.log4j.core.lookup.StrSubstitutor;

import java.util.Map;

public class App
{

    public static void main ( String[] args )
    {
        new App().run();
    }



    private static org.slf4j.Logger Log1 = org.slf4j.LoggerFactory.getLogger(App.class);
    private static org.apache.logging.log4j.Logger Log2 = org.apache.logging.log4j.LogManager.getLogger(App.class);

    private void run ()
    {
        LoggerContext loggerContext = LoggerContext.getContext();
        Configuration configuration = loggerContext.getConfiguration();
        StrSubstitutor strSubstitutor = configuration.getStrSubstitutor();
        StrLookup variableResolver = strSubstitutor.getVariableResolver(); // https://stackoverflow.com/a/37689415/2412477
        String pattern = variableResolver.lookup("app.log.pattern");
        PatternLayout patternLayout = PatternLayout.newBuilder().withPattern(pattern).build();

        Logger rootLogger = loggerContext.getRootLogger();
        Map<String, Appender> appenders = rootLogger.getAppenders();
//        Appender firstAppender = rootLogger.getAppenders().values().stream().findFirst().orElseThrow(() -> new IllegalStateException("No Appender found"));
//        PatternLayout layout = (PatternLayout)firstAppender.getLayout();
//
//        Appender helloConsoleAppender = rootLogger.getAppenders().get("HelloConsoleAppender");

//


        System.out.println("0-Hello, Stdout: Got " + appenders.size() + " appenders -- want 3!");

        Log1.trace("1-Hello, TRACE!");
        Log1.debug("1-Hello, DEBUG!");
        Log1.info("1-Hello, INFO!");
        Log1.warn("1-Hello, WARN!");
        Log1.error("1-Hello, ERROR!");

        Log2.trace("2-Hello, TRACE!");
        Log2.debug("2-Hello, DEBUG!");
        Log2.info("2-Hello, INFO!");
        Log2.warn("2-Hello, WARN!");
        Log2.error("2-Hello, ERROR!");
    }



}
