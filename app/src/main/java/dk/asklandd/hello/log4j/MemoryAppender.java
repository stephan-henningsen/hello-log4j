package dk.asklandd.hello.log4j;

import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.io.Serializable;
import java.util.Vector;

@Plugin(name = "Memory", category = Core.CATEGORY_NAME, elementType = Appender.ELEMENT_TYPE)
public class MemoryAppender extends AbstractAppender
{

    private Vector<LogEvent> events = new Vector<>();

    public MemoryAppender ( String name, Filter filter, Layout<? extends Serializable> layout )
    {
        super(name, filter, layout, true, Property.EMPTY_ARRAY);
    }

    @PluginFactory
    public static MemoryAppender createAppender (
        @PluginAttribute("name") String name,
        @PluginElement("Filter") Filter filter,
        @PluginElement("PatternLayout") PatternLayout patternLayout )
    {
        return new MemoryAppender(name, filter, patternLayout);
    }

    @Override
    public void append ( LogEvent event )
    {
        events.add(event);
    }

    public Vector<LogEvent> getEvents ()
    {
        return events;
    }

}


